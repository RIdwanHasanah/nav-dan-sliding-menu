package com.indonesia.ridwan.navigationdrawerdanslidingmenu;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    //Within wich the entire activity is enclosed
    private DrawerLayout mDrawerLayout;
    //ListView represents Navigation Drawer
    private ListView mDrawerList;
    //ActionBarDrawerToggle indicates the presence oj Navigation Drawer in the action bar
    private ActionBarDrawerToggle mDrawerToggle;
    //Title of the action bar
    private String mTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = "Jadwal Pelajaran";
        getActionBar().setTitle(mTitle);

        // Getting reference to the ActionBarDrawerToggle
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.drawer_list);


        //Getting refrence to the Drawer layput
        mDrawerToggle = new ActionBarDrawerToggle(this,mDrawerLayout,R.drawable.ic_drawer,R.string.drawer_open,R.string.drawer_close){

            // Caled when drawer is closed
            @Override
            public void onDrawerClosed(View drawerView) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu();
            }

            //Called when drawer is opened

            @Override
            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle("Jadwal_Pelajaran");
                invalidateOptionsMenu();
            }
        };

        //seting DrawerTOggle on DrawerLayout
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        //Creating an ArrayAdapter to add items to the listview mDrawerlist
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getBaseContext(),
                R.layout.drawer_list_item,getResources().getStringArray(R.array.menus));

        //Setting the adapter on mDrawerlist
        mDrawerList.setAdapter(adapter);

        //Enabling Home button
        getActionBar().setHomeButtonEnabled(true);

        //Enabling up navigation
        getActionBar().setDisplayHomeAsUpEnabled(true);

        //setting item click listener for the listview mDrawerList
        mDrawerList.setOnClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick (AdapterView < ? > parent, View view,int position, long id){

                //Getting an array of river
                String[] menuItms = getResources().getStringArray(R.array.menus);

                //Currently selected river
                mTitle = menuItms[position];

                if (mTitle.equals("Tambah Jadwal")) {
                    FragmentTmabahJadwal framentone = new FragmentTmabahJadwal();
                    FragmentManager fragmentManager = getFragmentManager();

                    //Creating selected river
                    FragmentTransaction ft = fragmentManager.beginTransaction();

                    //adding a fragment transaction
                    ft.replace(R.id.content_frame, framentone);

                    //Committing the transaction
                    ft.commit();
                } else {
                    //Creating a fragment onbject
                    ListFragment rFragment = new ListFragment();

                    //Passing aselected item information to fragment

                    Bundle data = new Bundle();
                    data.putInt("position", position);
                    //data.putString("url, getUrl(position)");
                    rFragment.setArguments(data);
                    //Getting refrence to the Fragementmanajer
                    FragmentManager fragmentManajer = getFragmentManager();
                    //Creating a fragment transaksion
                    FragmentTransaction ft = fragmentManajer.beginTransaction();
                    //adding a fragememt to the fragment transaction
                    ft.replace(R.id.content_frame, rFragment);
                    //Comitting the transaction
                    ft.commit();
                }
                //Closing the drawer
                mDrawerLayout.closeDrawer(mDrawerList);

            }

        });
    }
}
