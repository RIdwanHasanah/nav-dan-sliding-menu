package com.indonesia.ridwan.navigationdrawerdanslidingmenu.Adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.indonesia.ridwan.navigationdrawerdanslidingmenu.Jadwal;
import com.indonesia.ridwan.navigationdrawerdanslidingmenu.R;

import java.util.ArrayList;

/**
 * Created by hasanah on 7/25/16.
 */
public class AdapterJadwal extends ArrayAdapter <Jadwal> {
    private Activity activity;
    private ArrayList<Jadwal> lJadwal;
    private static LayoutInflater inflater = null;

    public AdapterJadwal (Activity activity, int textViewResourceId, ArrayList<Jadwal> al_Jadwal){
        super(activity, textViewResourceId, al_Jadwal);
        try{
            this.activity = activity;
            this.lJadwal = al_Jadwal;
            inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }
        catch (Exception e){

        }
    }

    public int getCount(){
        return  lJadwal.size();
    }

    public long getItemId (int position ){
        return position;
    }

    public  static class ViewHolder {
        public TextView jam;
        public TextView pelajaran;
    }

    public View getView(int position, View concertView , ViewGroup parent){

        View vi = concertView;
        final ViewHolder holder;
        try{
            if (concertView==null){
                vi = inflater.inflate(R.layout.row_jadwal,null);
                holder = new ViewHolder();

                holder.jam = (TextView) vi.findViewById(R.id.textViewjam);
                holder.pelajaran = (TextView) vi.findViewById(R.id.textViewPelajaran);

                vi.setTag(holder);
            }
            else {
                holder = (ViewHolder) vi.getTag();
            }
            holder.jam.setText(lJadwal.get(position).jam);
            holder.pelajaran.setText(lJadwal.get(position).pelajaran);
        }
        catch (Exception e){

        }
        return vi;
    }

}
